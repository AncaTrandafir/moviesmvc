﻿//using Movies.Models;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Threading.Tasks;


//namespace Movies.ViewModels
//{
//    public class MoviePostModel
//    {
//        public long Id { get; set; }

//        public string Title { get; set; }

//        public string Description { get; set; }

//        public Genre Genre { get; set; }   // String pt ca voi compara cu un String

//        public int Duration { get; set; }

//        public string YearOfRelease { get; set; }

//        public string Director { get; set; }

//        public string DateAdded { get; set; }

//        [Range(1, 10)]
//        public int Rating { get; set; }

//        [Range(0, 1)]
//        public short Watched { get; set; }


//        public static Movie ToMovie(MoviePostModel movie)
//        {
//            Genre genre = Genre.Adventure;

//            if (movie.Genre.Equals("Comedy"))
//                genre = Genre.Comedy;

//            if (movie.Genre.Equals("Horror"))
//                genre = Genre.Horror;

//            if (movie.Genre.Equals("SciFi"))
//                genre = Genre.SciFi;

//            return new Movie
//            {
//                Title = movie.Title,
//                Description = movie.Description,
//                Genre = genre, // genre de mai sus
//                Duration = movie.Duration,
//                YearOfRelease = movie.YearOfRelease,
//                Director = movie.Director,
//                DateAdded = movie.DateAdded,
//                Rating = movie.Rating,
//                Watched = movie.Watched
//            };
//        }
//    }
//}
