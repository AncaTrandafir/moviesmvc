﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Models
{
    public static class MovieDbSeeder
    {
        //
      public static void Initialize(MoviesDbContext context)
       // public static void Initialize(IServiceProvider serviceProvider)

        {
             context.Database.EnsureCreated(); // db automatically recreates itself to match my changes

         //   using (var context = new MoviesDbContext(
         //      serviceProvider.GetRequiredService<
          //         DbContextOptions<MovieDbContext>>()))


                // Look for any movies
                // If there are any movies in the DB, the seed initializer returns and no movies are added.
                if (context.Movies.Any())
            {
                return;     // DB has been seeded
            }

            //  context.Movies.AddRange(

            var movies = new Movie[] {
                new Movie
                {
                  //  ID = 1,
                    Title = "Stapanul inelelor",
                    Description = "Fight between good and evil",
                    Genre = 0,
                    Duration = 120,
                    YearOfRelease = 2010,
                    Director = "Steven Spielberg",
                    DateAdded = DateTime.Parse("03.03.2019"),
                    Rating = 10,
                    Watched = 1
                },

                new Movie
                {
                 //   ID = 2,
                    Title = "Superman",
                    Description = "Fight between good and evil",
                    Genre = 0,
                    Duration = 120,
                    YearOfRelease = 1970,
                    Director = "Ronald Figgure",
                    DateAdded = DateTime.Parse("03.03.2017"),
                    Rating = 10,
                    Watched = 1
                },
            };
              context.Movies.AddRange(movies);
              context.SaveChanges();


         //   context.SaveChanges();  // commit transaction



           // var comments = new Comment[]
           //{
           //    new Comment
           //    {
           //        MovieID = movies.Single(m => m.Title == "Stapanul inelelor").ID,
           //        Text="Acesta este un comm",
           //        Important=0
           //    },

           // //new Comment{MovieID=1,Text="Acesta este un comm",Important=0},
           // //new Comment{MovieID=2,Text="Acesta este un comm",Important=1},
           // //new Comment{MovieID=2,Text="Acesta este alt comm",Important=1},
           // //new Comment{MovieID=2,Text="Acesta este inca un comm",Important=0},
           // //new Comment{MovieID=2,Text="Acesta este ultimul comm",Important=0},
           // //new Comment{MovieID=1,Text="Acesta este inca comm",Important=2},

           //};
           // //foreach (Comment c in comments)
           // //{
           // //    context.Comments.Add(c);
           // //}
           // //context.SaveChanges();

           // context.Comments.AddRange(comments);
           // context.SaveChanges();

        }
    }

}
