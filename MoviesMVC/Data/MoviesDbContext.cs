﻿using Microsoft.EntityFrameworkCore;
using MoviesMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Models
{
    // DbContext = Unit of Work
    public class MoviesDbContext : DbContext
    {
        public MoviesDbContext(DbContextOptions<MoviesDbContext> options)
            : base(options)
        {       // the default constructor with DbContextOptions
        }

        // DbSet = Repository
        // DbSet = O tabela din baza de date
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Comment> Comments { get; set; }




        // creez tabele

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().ToTable(" Movies");
            modelBuilder.Entity<Comment>().ToTable("Comments");
          
            modelBuilder.Entity<Comment>()
                   .HasKey(c => new
                   {
                       c.CommentID,
                       c.MovieID        // unui id comment ii corespunde un id de movie
                   });

            base.OnModelCreating(modelBuilder);     // ??????????

        }
    }
}

