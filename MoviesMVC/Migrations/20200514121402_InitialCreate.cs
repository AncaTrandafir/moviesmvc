﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesMVC.Migrations
{
    public partial class InitialCreate : Migration
    {


        // The Up method creates the Movie table and configures Id as the primary key. 
        // The Down method reverts the schema changes made by the Up migration.

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: " Movies",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 150, nullable: true),
                    Genre = table.Column<int>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    YearOfRelease = table.Column<int>(nullable: false),
                    Director = table.Column<string>(maxLength: 30, nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Watched = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ Movies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CommentID = table.Column<int>(nullable: false),
                    MovieID = table.Column<long>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    Important = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => new { x.CommentID, x.MovieID });
                    table.ForeignKey(
                        name: "FK_Comments_ Movies_MovieID",
                        column: x => x.MovieID,
                        principalTable: " Movies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_MovieID",
                table: "Comments",
                column: "MovieID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: " Movies");
        }
    }
}
