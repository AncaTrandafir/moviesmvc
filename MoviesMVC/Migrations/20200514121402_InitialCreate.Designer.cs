﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Movies.Models;

namespace MoviesMVC.Migrations
{
    [DbContext(typeof(MoviesDbContext))]
    [Migration("20200514121402_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Movies.Models.Comment", b =>
                {
                    b.Property<int>("CommentID");

                    b.Property<long>("MovieID");

                    b.Property<short>("Important");

                    b.Property<string>("Text");

                    b.HasKey("CommentID", "MovieID");

                    b.HasIndex("MovieID");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Movies.Models.Movie", b =>
                {
                    b.Property<long>("ID");

                    b.Property<DateTime>("DateAdded");

                    b.Property<string>("Description")
                        .HasMaxLength(150);

                    b.Property<string>("Director")
                        .HasMaxLength(30);

                    b.Property<int>("Duration");

                    b.Property<int?>("Genre");

                    b.Property<int>("Rating");

                    b.Property<string>("Title")
                        .HasMaxLength(50);

                    b.Property<short>("Watched");

                    b.Property<int>("YearOfRelease");

                    b.HasKey("ID");

                    b.ToTable(" Movies");
                });

            modelBuilder.Entity("Movies.Models.Comment", b =>
                {
                    b.HasOne("Movies.Models.Movie", "Movie")
                        .WithMany("Comments")
                        .HasForeignKey("MovieID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
