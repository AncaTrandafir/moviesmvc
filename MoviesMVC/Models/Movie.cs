using Movies.Models;
using MoviesMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Movies.Models
{

    public enum Genre
    {
        Adventure,
        Comedy,
        Horror,
        SciFi
    }


    public class Movie
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Number")]
        public long ID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        [StringLength(150, MinimumLength = 3)]
        public string Description { get; set; }

        // [EnumDataType(typeof(Genre))]
        [DisplayFormat(NullDisplayText = "No genre")]
        public Genre? Genre { get; set; }

        public int Duration { get; set; }

        [Range(1900, 2020)]
        public int YearOfRelease { get; set; }

        [StringLength(30, MinimumLength = 3)]
        public string Director { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateAdded { get; set; }

        [Range(1, 10)]
        public int Rating { get; set; }

        [Range(0, 1)]
        public short Watched { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
