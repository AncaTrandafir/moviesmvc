﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Models
{
    public class Comment
    {

        public int CommentID { get; set; }  // de specificat cu classNameID

        public long MovieID { get; set; } // foreign key, corresponding navigation property is Movie

        public string Text { get; set; }
        public short Important { get; set; }  // in mySql nu am tipul bool, ci smallInt

        public Movie Movie { get; set; }
    }
}
