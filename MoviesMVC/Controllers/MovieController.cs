﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using Movies.Models;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
//using Movies;
//using Microsoft.EntityFrameworkCore;

//namespace HotelMng.Controllers
//{
//    [ApiController]
//    [Route("[controller]")]
//    public class MoviesController : ControllerBase
//    {

//        private readonly MoviesDbContext _context;


//        public MoviesController(MoviesDbContext context)
//        {
//            _context = context;

//        }



//        // GET: Movies
//        [HttpGet]
//         public async Task<ActionResult<IEnumerable<Movie>>> GetMovies()
//        {

//              return await _context.Movies.ToListAsync();
//        }  




//        // GET: Movie/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<Movie>> GetMovie(long id)
//        {
//            var movie = await _context.Movies.FindAsync(id);

//            if (movie == null)
//            {
//                return NotFound();
//            }
//            return movie;
//        }

//        // GET: Movies/filter?from=a&to=b
//        [HttpGet("filter")]
//        public IOrderedQueryable<Movie> GetFilteredMovies(String from, String to )
//        {
//            DateTime fromDate =  DateTime.ParseExact(from, "dd.MM.yyyy", null);
//            DateTime toDate = DateTime.ParseExact(to, "dd.MM.yyyy", null);

//            // Linq
//            var results = _context.Movies.Where(o => fromDate.CompareTo(DateTime.ParseExact(o.DateAdded, "dd.MM.yyyy", null)) == -1 && toDate.CompareTo(DateTime.ParseExact(o.DateAdded, "dd.MM.yyyy", null)) == -1);

//            var sortedResultsByYearOfRelease = results.OrderBy(o => DateTime.ParseExact(o.YearOfRelease, "yyyy", null));  

//            return sortedResultsByYearOfRelease;  // type IOrderedQueryable
//        }






//        // PUT: Movie/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutMovie(long id, Movie movie)
//        {
//            if (id != movie.Id)
//            {
//                return BadRequest();
//            }

//            if (!MovieExists(id))
//            {
//                return NotFound();
//            }


//            // validator
//            validateAttributes(movie);


//            _context.Entry(movie).State = EntityState.Modified;

//            await _context.SaveChangesAsync();

//            return Ok(movie);
//        }






//        // POST: Movie
//        [HttpPost]
//        public async Task<IActionResult> PostMovie(Movie movie)
//        {

//            // validator
//            validateAttributes(movie);

//            _context.Movies.Add(movie);
//            await _context.SaveChangesAsync();


//            // return CreatedAtAction("GetMovie", new { id = movie.id }, movie);
//            return Ok(movie);
//        }






//        // DELETE: /5
//        [HttpDelete("{id}")]
//        public async Task<ActionResult<Movie>> DeleteMovie(long id)
//        {
//            var movie = await _context.Movies.FindAsync(id);
//            if (movie == null)
//            {
//                return NotFound();
//            }

//            _context.Movies.Remove(movie);
//            _context.Entry(movie).State = EntityState.Deleted;
//            await _context.SaveChangesAsync();

//            return movie;
//        }



//        private bool MovieExists(long id)
//        {
//            return _context.Movies.Any(e => e.Id == id);
//        }




//        // validate Date format
//        public static bool IsValidDate(string value, string[] dateFormats)
//        {
//            DateTime tempDate;
//            bool validDate = DateTime.TryParseExact(value, dateFormats, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out tempDate);
//            if (validDate)
//                return true;
//            else
//                return false;
//        }


//        // validate properties
//        public static void validateAttributes(Movie movie)
//        {
//            String[] dateFormats = { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" };
//            if (!IsValidDate(movie.DateAdded, dateFormats))
//            {
//                throw new ArgumentException("Date format not valid");
//            }

//            if (string.IsNullOrEmpty(movie.DateAdded) || string.IsNullOrEmpty(movie.Description) || string.IsNullOrEmpty(movie.Director) || string.IsNullOrEmpty(movie.Title) || string.IsNullOrEmpty(movie.YearOfRelease))
//            {
//                throw new ArgumentException("All fields are mandatory");
//            }

//            if (movie.Duration == 0)
//            {
//                throw new ArgumentException("Duration must be greater than 0 minutes");
//            }

//            if (movie.Rating < 1 || movie.Rating > 10)
//            {
//                throw new ArgumentException("Rating must be a number between 1 and 10");
//            }

//            //DateTime yearOfRelease = DateTime.ParseExact(movie.YearOfRelease, "yyyy", null);

//            //if (yearOfRelease.CompareTo(1900) == -1  || yearOfRelease.CompareTo(DateTime.Now) > 1)
//            //{
//            //    throw new ArgumentException("Year of release invalid");
//            //}
//        }






//    }
//}

